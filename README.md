# Sistema de Console TO DO

Este é um console interativo, com opções que se podem selecionar com as teclas direcionais, números e com a barra de espaço quando há muitas opções.

Utilizo neste projeto:

- stdin
- stdout
- Ciclos
- Inquirer
- Classes JavaScript
- Arquivo JSON
- async e await
- Transformações

Esta é uma aplicação real que pode ser muito útil muito quando tenham que criar alguna aplicação de console.
