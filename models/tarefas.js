const Tarefa = require("./tarefa");

class Tarefas {
  _lista = {};

  get listaArr() {
    const lista = [];

    Object.keys(this._lista).forEach((key) => {
      const tarefa = this._lista[key];
      lista.push(tarefa);
    });

    return lista;
  }

  constructor() {
    this._lista = {};
  }

  excluirTarefa(id = "") {
    if (this._lista[id]) {
      delete this._lista[id];
    }
  }

  criarTarefa(desc = "") {
    const tarefa = new Tarefa(desc);
    this._lista[tarefa.id] = tarefa;
  }

  carregarTarefasFromArray(tarefas = []) {
    tarefas.forEach((tarefa) => {
      if (tarefa) this._lista[tarefa.id] = tarefa;
    });
  }

  listaCompleta() {
    console.log();
    this.listaArr.forEach((tarefa, i) => {
      if (tarefa) {
        const idx = `${i + 1}`.green;
        const { desc, completadoEm } = tarefa;
        const estado = completadoEm ? "Completa".green : "Pendente".red;

        console.log(`${idx} ${desc} :: ${estado}`);
      }
    });
  }

  listarPendentesCompletas(completas = true) {
    console.log();
    let contador = 0;
    this.listaArr.forEach((tarefa) => {
      const { desc, completadoEm } = tarefa;
      const estado = completadoEm ? "Completa".green : "Pendente".red;

      if (completas) {
        if (completadoEm) {
          contador += 1;
          console.log(
            `${(contador + ".").green} ${desc} :: ${completadoEm.green}`
          );
        }
      } else {
        if (!completadoEm) {
          contador += 1;
          console.log(`${(contador + ".").green} ${desc} :: ${estado}`);
        }
      }
    });
  }

  toggleCompletas(ids = []) {
    ids.forEach((id) => {
      const tarefa = this._lista[id];
      if (!tarefa.completadoEm) tarefa.completadoEm = new Date().toISOString();
    });

    this.listaArr.forEach((tarefa) => {
      if (!ids.includes(tarefa.id)) this._lista.completadoEm = null;
    });
  }
}

module.exports = Tarefas;
