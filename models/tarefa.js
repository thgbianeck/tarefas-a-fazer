const { v4: uuidiv4 } = require("uuid");

class Tarefa {
  id = "";
  desc = "";
  completadoEm = null;

  constructor(desc) {
    this.id = uuidiv4();
    this.desc = desc;
    this.completadoEm = null;
  }
}

module.exports = Tarefa;
