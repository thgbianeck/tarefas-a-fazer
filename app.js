const { guardarDB, lerDB } = require("./helpers/guardarArquivo");
const {
  inquirerMenu,
  pausa,
  lerInput,
  listaTarefasExcluir,
  confirmar,
  mostrarListaCheckList,
} = require("./helpers/inquirer");
const Tarefas = require("./models/tarefas");

require("colors");

const main = async () => {
  let opt = "";
  const tarefas = new Tarefas();

  const tarefasDB = lerDB();

  if (tarefasDB) tarefas.carregarTarefasFromArray(tarefasDB);

  do {
    opt = await inquirerMenu();

    switch (opt) {
      case "1": // Criar tarefa
        const desc = await lerInput("Descrição:");
        tarefas.criarTarefa(desc);
        break;
      case "2": // Listar tarefas
        tarefas.listaCompleta();
        break;
      case "3": // Listar tarefas completadas
        tarefas.listarPendentesCompletas(true);
        break;
      case "4": // Listar tarefas pendentes
        tarefas.listarPendentesCompletas(false);
        break;
      case "5": // Completar tarefas
        const ids = await mostrarListaCheckList(tarefas.listaArr);
        console.log(ids);
        tarefas.toggleCompletas(ids);
        break;
      case "6": // Excluir tarefa
        const id = await listaTarefasExcluir(tarefas.listaArr);
        if (id !== "0") {
          const ok = await confirmar("Tem certeza?");
          if (ok) {
            tarefas.excluirTarefa(id);
            console.log("Tarefa Excluída");
          }
        }
        break;
    }

    guardarDB(tarefas.listaArr);
    await pausa();
  } while (opt != "0");

  //   pausa();
};

main();
