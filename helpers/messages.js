require("colors");

const mostrarMenu = () => {
  return new Promise((resolve) => {
    console.clear();
    console.log("==========================".green);
    console.log("  Selecione uma opcao".green);
    console.log("==========================\n".green);

    console.log(`${"1.".green} Criar tarefa`);
    console.log(`${"2.".green} Listar tarefas`);
    console.log(`${"3.".green} Listar tarefas completadas`);
    console.log(`${"4.".green} Listar tarefas pendentes`);
    console.log(`${"5.".green} Completar tarefa(s)`);
    console.log(`${"6.".green} Excluir tarefa`);
    console.log(`${"0.".green} Sair \n`);

    const readline = require("readline").createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    readline.question("Selecione uma opção: ", (opt) => {
      readline.close();
      resolve(opt);
    });
  });
};

const pausa = () => {
  return new Promise((resolve) => {
    const readline = require("readline").createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    readline.question(
      `\nPressione ${"ENTER".green} para continuar.\n`,
      (opt) => {
        readline.close();
        resolve();
      }
    );
  });
};

module.exports = {
  mostrarMenu,
  pausa,
};
