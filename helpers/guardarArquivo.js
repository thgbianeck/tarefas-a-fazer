const fs = require("fs");

const arquivo = "./db/data.json";

const guardarDB = (data) => {
  fs.writeFileSync(arquivo, JSON.stringify(data));
};

const lerDB = () => {
  if (!fs.existsSync(arquivo)) return null;

  const info = fs.readFileSync(arquivo, { encoding: "utf-8" });
  if (!info) return;
  const data = JSON.parse(info);

  return data;
};

module.exports = {
  guardarDB,
  lerDB,
};
