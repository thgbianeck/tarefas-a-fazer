const inquirer = require("inquirer");
const { async } = require("rxjs");
require("colors");

const perguntas = [
  {
    type: "list",
    name: "opcao",
    message: "O Que desejas fazer?",
    choices: [
      {
        value: "1",
        name: `${"1.".green} Criar tarefa`,
      },
      {
        value: "2",
        name: `${"2.".green} Listar tarefas`,
      },
      {
        value: "3",
        name: `${"3.".green} Listar tarefas completadas`,
      },
      {
        value: "4",
        name: `${"4.".green} Listar tarefas pendentes`,
      },
      {
        value: "5",
        name: `${"5.".green} Completar tarefa(s)`,
      },
      {
        value: "6",
        name: `${"6.".green} Excluir tarefa`,
      },
      {
        value: "0",
        name: `${"0.".green} Sair`,
      },
    ],
  },
];

const inquirerMenu = async () => {
  console.clear();
  console.log("==========================".green);
  console.log("  Selecione una opcao".white);
  console.log("==========================\n".green);

  const { opcao } = await inquirer.prompt(perguntas);

  return opcao;
};

const pausa = async () => {
  const question = [
    {
      type: "input",
      name: "enter",
      message: `Pressione ${"ENTER".green} para continuar`,
    },
  ];

  console.log("\n");
  await inquirer.prompt(question);
};

const lerInput = async (message) => {
  const question = [
    {
      type: "input",
      name: "desc",
      message,
      validate(value) {
        if (value.length === 0) {
          return "Por favor, insira um valor";
        }
        return true;
      },
    },
  ];

  const { desc } = await inquirer.prompt(question);
  return desc;
};

const listaTarefasExcluir = async (tarefas = []) => {
  const choices = tarefas.map((tarefa, i) => {
    const idx = `${i + 1}.`.green;

    return {
      value: tarefa.id,
      name: `${idx} ${tarefa.desc}`,
    };
  });

  choices.unshift({
    value: "0",
    name: "0.".green + " Cancelar",
  });

  const perguntas = [
    {
      type: "list",
      name: "id",
      message: "Excluir",
      choices,
    },
  ];

  const { id } = await inquirer.prompt(perguntas);
  return id;
};

const confirmar = async (message) => {
  const question = [
    {
      type: "confirm",
      name: "ok",
      message,
    },
  ];

  const { ok } = await inquirer.prompt(question);
  return ok;
};

const mostrarListaCheckList = async (tarefas = []) => {
  const choices = tarefas.map((tarefa, i) => {
    const idx = `${i + 1}.`.green;

    return {
      value: tarefa.id,
      name: `${idx} ${tarefa.desc}`,
      checked: tarefa.completadoEm ? true : false,
    };
  });

  const pergunta = [
    {
      type: "checkbox",
      name: "ids",
      message: "Seleções",
      choices,
    },
  ];

  const { ids } = await inquirer.prompt(pergunta);
  return ids;
};

module.exports = {
  inquirerMenu,
  pausa,
  lerInput,
  listaTarefasExcluir,
  confirmar,
  mostrarListaCheckList,
};
